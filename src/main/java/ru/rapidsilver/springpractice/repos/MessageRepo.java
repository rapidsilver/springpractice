package ru.rapidsilver.springpractice.repos;

import org.springframework.data.mongodb.repository.MongoRepository;

import org.springframework.stereotype.Repository;
import ru.rapidsilver.springpractice.domain.Message;



@Repository
public interface MessageRepo extends MongoRepository<Message, String> {

}
