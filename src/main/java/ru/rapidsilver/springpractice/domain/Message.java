package ru.rapidsilver.springpractice.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.text.SimpleDateFormat;
import java.util.Date;

@Document
public class Message {
    @Id
    private String id;
    private String title;
    private String textMessage;
    private String date;
    private String imageName;

    public Message(String title, String textMessage) {
        this.title = title;

        this.textMessage = textMessage;
        Date date = new Date();
        String d = String.format("%tm %<tB, %<tY", date);
        this.date = d;

    }

    public Message() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
