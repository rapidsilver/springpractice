package ru.rapidsilver.springpractice;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.rapidsilver.springpractice.domain.Message;
import ru.rapidsilver.springpractice.repos.MessageRepo;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

@Controller
public class BlogController {
    @Autowired
    private MessageRepo messageRepo;

    @Value("${upload.path}")
    private String uploadPath;

    @GetMapping("/message/{id}")
    public  String message(Model model, @PathVariable(name = "id") String id) {
        Message message = messageRepo.findById(id).get();

        model.addAttribute("message", message);

        return "message";
    }

    @GetMapping("/")
    public  String main(Map<String, Object> model) {
        Iterable<Message> messages = messageRepo.findAll();
        long count = messageRepo.count();

        model.put("count", count);
        model.put("messages", messages);

        return "main";
    }

    @GetMapping("/add")
    public  String addpage() {
        return "add";
    }


    @PostMapping("/add")
    public String add(@RequestParam String title,
                      @RequestParam String textMessage,
                      @RequestParam("file") MultipartFile file,
                      Map<String, Object> model) throws IOException {
        Message message = new Message(title, textMessage);
        if (file != null) {
            File upDir = new File(uploadPath);

            if (!upDir.exists()) {
                upDir.mkdir();
            }
            String uuidFile = UUID.randomUUID().toString();
            String filename = uuidFile + "." + file.getOriginalFilename();
            file.transferTo(new File(uploadPath +"/" + filename));
            message.setImageName(filename);

        }

        messageRepo.save(message);

        return  "redirect:/";
    }
}
